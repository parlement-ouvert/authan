# AuthAN

> AuthAN is a Node library that allows french deputies, their assistants and the administration members to authenticate using their Assemblée nationale account.

AuthAN is a first step toward the french [Open Parliament Platform](https://forum.parlement-ouvert.fr): It allows the development of applications that use the [Assemblée nationale](http://www.assemblee-nationale.fr/) extranet and require user authentication.

AuthAN is free and open source software, developped by the french Member of Parliament [Paula Forteza](https://forteza.fr) and her team.

## Usage

See [/src/examples](/src/examples).
