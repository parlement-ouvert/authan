/**
 * AuthAN -- Authentication library for French Assemblée nationale
 * By: Paula Forteza <paula@forteza.fr>
 *     Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/authan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Load different configurations (for development, production, etc).
const configFilename = process.env.NODE_ENV || "development"
const config = require("../../config/" + configFilename).default

export default config
